package com.nick.software.medicine.storage.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Table("storage")
public class Storage {

    @Id
    @Column("id")
    private long id;

    @NotBlank(message = "must be filled in")
    @Column("name")
    private String name;

    @NotBlank(message = "must be filled in")
    @Column("description")
    private String description;

    @Min(value = 0,message = "cannot be less than 0")
    @Column("amount")
    private long amount;
}
